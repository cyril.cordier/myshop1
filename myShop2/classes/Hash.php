<?php 

// password 
class Hash{
    public static function make($string){
        return password_hash($string, PASSWORD_BCRYPT);

    }

    public static function unique(){ // if password are same protect 
        return self::make(uniqid());
    }
    
    public static function verify($password, $hash){
       return password_verify($password,$hash);
    }
}