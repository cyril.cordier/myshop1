<?php 


class User{
    private $_db;
    private $_data;
    private $_sessionName;
    private $_isLoggedIn;
    private $_cookieName;
    
    public function __construct($user = null){
    $this->_db = DB::getInstance();
    $this->_sessionName = Config::get('session/session_name');
    $this->_cookieName = Config::get('remember/cookie_name');

    if(!$user){
        if(Session::exists($this->_sessionName)){
            $user = Session::get($this->_sessionName);

            if($this->find($user)){
                $this->_isLoggedIn = true;
            }
            else{
                //logout;
            }
        }elseif(Cookie::exists($this->_cookieName)){
            $user = Cookie::get($this->_cookieName);
            if($this->find($user)){
                $this->_isLoggedIn = true;
            }else{
                //logout;
            }
        }
    }else{
        $this->find($user);
    }
    }

    public function create($fields = array()){
        if(!$this->_db->insert('users', $fields)){
            throw new Exception ('There was a problem account not created !');

        }
    }

    public function find($user = null){
        if($user){
            $field = 'email';
            $data = $this->_db->get('users', array($field, '=', $user));

            if($data->count()){
                $this->_data = $data->first();
                return true;
            }
        }
        return false;
    }
    public function login($email = null,$password = null,$remember){
        $user = $this->find($email);
        if($user){
            if(Hash::verify($password,$this->data()->password) == true){
  
               if($remember){
                Cookie::put($this->_cookieName,$this->data()->email,Config::get('remember/cookie_expiry'));
               }else{
                Session::put($this->_sessionName, $this->data()->email);
               }
               return true;
            }
        }

        return false;
    }
    public function data(){
        return $this->_data;
    }
    public function isLoggedIn(){
        
        return $this->_isLoggedIn; 
    }
    public function logout(){
        Session::delete($this->_sessionName);
        Cookie::delete($this->_cookieName);
    }
}