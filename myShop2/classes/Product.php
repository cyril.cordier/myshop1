<?php 

class Product{
    private $_db;
    private $_data;

    public function __construct($user = null){
        $this->_db = DB::getInstance();
}
public function find($product = null){
    if($product){
        $field = 'name';
        $data = $this->_db->get('products', array($field, '=', $product));

        if($data->count()){
            $this->_data = $data->first();
            return true;
        }
    }
    return false;
}
public function createProduct($fields = array()){
    if(!$this->_db->insert('products', $fields)){
        throw new Exception ('There was a problem product not created !');

    }
}
public function data(){
    return $this->_data;
}

}