<?php 

require_once '../core/init.php';

$admin = new User;

    if($admin->data()->admin == true){

$user = DB::getInstance()->getAll('SELECT *','users');

$result = $user->results();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
<link rel="stylesheet" href="../css/style.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <title>Admin</title>
</head>
<body>
    <div class="container">
    
    <h1 class="alert-success"> HELLO <?php echo $admin->data()->username ?> ! </h1>
    <h2>WELCOME TO YOUR ADMIN CRUD <h2>
    <a href="showProducts.php"><button class="btn-block btn-lg btn-primary" type = "submit" name="product_list">SHOW PRODUCTS</button></a>
    <a href="showUsers.php"><button class="btn-block btn-lg btn-dark" type = "submit" name="product_list">SHOW USERS</button></a>
    <a href="logout.php"><button class="btn-block btn-lg btn-danger" type = "submit" name="product_list">LOG OUT</button></a>
    <a href="index.php"><button class="btn-block btn-lg btn-success" type = "submit" name="product_list">SHOP</button></a>

    </div>
</body>
</html>
        
<?php
    }else{
    Redirect::to('index.php');
}

?>

