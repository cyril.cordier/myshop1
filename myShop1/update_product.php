<?php

        
$jsondecode=json_decode($_COOKIE["infos_user"], true);

if($jsondecode["autoriser"]!= "oui"){
    header("location:index.php");
    exit;
}
    
    @$new_name=$_POST["name"];
    @$new_price=$_POST["price"];
    @$new_category_id=$_POST["category_id"];
    @$new_description=$_POST["description"];
    @$new_picture=$_POST["picture"];
    @$valider=$_POST["valider"];
    $testname=0;
    $erreur_name;
    
    CONST ERROR_LOG_FILE='errors.log';
        $DB_name='my_shop';
        $DB_user='cyril';
        $DB_password ='cyril';
        $DB_host= 'localhost';
        $DB_port= 3306;
        $DB_table='products';

    try{       
        
        $connect = new PDO("mysql:host=".$DB_host.";port=".$DB_port.";dbname=".$DB_name, $DB_user, $DB_password);
        
        $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if($connect){
            
            
            $requete=$connect->prepare("SELECT * FROM products WHERE id=:id");
            
            $requete->execute([
                'id'=>$_GET['update_product']
                ]);
                
            $ligne = $requete->fetch(PDO::FETCH_ASSOC);
            $info_product=array('name'=>$ligne['name'], 'price'=>$ligne['price'], 'category_id'=>$ligne['category_id'], 'description'=>$ligne['description'], 'picture'=>$ligne['picture']);
        }
    }
    catch(PDOException $e){
        
        $error_connect = "Error connection to DB\n";
        error_log(date("Y-m-d H:i")." -> ".$error_connect, 3,ERROR_LOG_FILE);
        echo $error_connect;
        $error_message = "PDO ERROR : ".$e->getMessage()." storage in ".ERROR_LOG_FILE."\n";
        error_log(date("Y-m-d H:i")." -> ".$error_message, 3,ERROR_LOG_FILE);
        exit;
    }

    function modifyProduct(array $update, $ligne, $connect)
    { 
        $filename=basename($_FILES['file']['name']);
        //var_dump($filename);


        $uploaddir = "./pictures/";
        $uploadfile = $uploaddir . $filename;
        //move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);
        //echo '<pre>';
        if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
            echo "Le fichier est valide, et a été téléchargé
                avec succès.\n";
            $update_picture=$filename;
        } else {
            //echo "Attaque potentielle par téléchargement de fichiers.
             //   Voici plus d'informations :\n";
            $update_picture=$ligne['picture'];
        }
        

        /*echo 'Voici quelques informations de débogage :';
        print_r($_FILES);*/

        //echo '</pre>';

        $connection=$connect->prepare("UPDATE products SET 
        name=:name, 
        price=:price, 
        category_id=:category_id,
        description=:description,
        picture=:picture 
        WHERE id=:id;");
        $connection->execute([
            'id'=>$_GET['update_product'],
            'name'=>$update['name'],
            'price'=>$update['price'],
            'category_id'=>$update['category_id'],
            'description'=>$update['description'],
            'picture'=>$update_picture
        ]); 
        $info_product=array('name'=>$update['name'], 'price'=>$update['price'], 'category_id'=>$new_category_id, 'description'=>$new_description, 'picture'=>$new_picture);
        header("location:show_products.php");
    }
    if($_SERVER['REQUEST_METHOD']== 'POST')
    {
        if(isset($valider)){
            
            
            if(isset($new_name)&&(strlen($new_name)<3 || strlen($new_name)>100)){
                $erreur_name="Invalid name";
            }else{
                $update=array('name'=>$new_name, 'price'=>$new_price, 'category_id'=>$new_category_id, 'description'=>$new_description, 'picture'=>$new_picture);
                modifyProduct($update, $ligne, $connect);
            }
        }
    }

?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Update User</title>     

    </head>
    <body>
    <div>
        <?php
            if (!empty($erreur_name)){?>
            <div id="erreur name">
                <?=$erreur_name?>
            </div>
            <?php 
            }
            if (!empty($erreur_email)){?>
            <div id="erreur email">
                <?=$erreur_email?>
            </div>
            <?php 
            }
            if (!empty($erreur_password)){?>
                <div id="erreur password">
                    <?=$erreur_password?>
                </div>
            <?php 
            }?>
            
    </div>
            <style type="text/css">
            .calage{
                margin:0 20px 0;
                display:inline;
                width:150px;
                float:left;
            }
            .description{
                width:300px;
                height:300px;
                font-size:15px;
            }
            .see-picture{
                width:300px;
                height:300px;
            }
            </style>
        <br/>
        <form enctype="multipart/form-data"  method="post">
        <label for="name" class="calage">Name :</label><input type="text" placeholder="Name" name="name" value="<?=$info_product['name']?>"/><br/>
            <label for="price" class="calage">Price :</label><input type="text" placeholder="Price" name="price" value="<?=$info_product['price']?>"/><br/>
            <label for="category_id" class="calage">Category :</label><input type="text" placeholder="Category" name="category_id" value="<?=$info_product['category_id']?>"/><br/>
            <label for="description" class="calage">Description :</label><textarea placeholder="Description" name="description" class="description"><?=$info_product['description']?></textarea><br/>
            
            <label for="picture" class="calage">Picture :</label><img class="see-picture" alt="picture_product" src="./pictures/<?= $ligne['picture'];?>"/><br/>
            <label for="update_picture" class="calage">Update picture :</label><input type="hidden" name="MAX_FILE_SIZE" value="<?php echo MAX_SIZE; ?>" /><input type="file" placeholder="Picture" name="file"  value="<?=$info_product['picture']?>"/>
            <br/><br/> 

            <input type="submit" name="valider" value="Submit"/>
            <a href="show_products.php"><input type="button" name="show_products" value="Back to show_product page"/></a></br>  
        </form>
       

    </body>
</html>