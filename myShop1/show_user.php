<?php
    
    $jsondecode=json_decode($_COOKIE["infos_user"], true);

    if($jsondecode["autoriser"]!= "oui" || $jsondecode["admin"]!= 1){
        header("location:index.php");
        exit;
    }
    $DB_name='my_shop';
    $DB_user='cyril';
    $DB_password ='cyril';
    $DB_host= 'localhost';
    $DB_port= 3306;
    $DB_table='users';
    try{       
        
        $connect = new PDO("mysql:host=".$DB_host.";port=".$DB_port.";dbname=".$DB_name, $DB_user, $DB_password);
        
        $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if($connect){
            
            $sql="SELECT * FROM users";
            $requete=$connect->prepare($sql);
            
            $requete->execute();
                
            //$ligne = $requete->fetch(PDO::FETCH_ASSOC);
        
        

                //<?php
        //$pdo = new PDO('mysql:host='.$host.';dbname='.$db, $db_user, $db_password);
        //$sql = 'SELECT * FROM users';   
        //$req = $pdo->query($sql);  


            
            //$req->closeCursor();
        }
    



    }
    catch(PDOException $e){
    
        $error_connect = "Error connection to DB\n";
        error_log(date("Y-m-d H:i")." -> ".$error_connect, 3,ERROR_LOG_FILE);
        echo $error_connect;
        $error_message = "PDO ERROR : ".$e->getMessage()." storage in ".ERROR_LOG_FILE."\n";
        error_log(date("Y-m-d H:i")." -> ".$error_message, 3,ERROR_LOG_FILE);
        exit;
    }
    
    


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Show User page</title>
        <div> <h1>Hello <?=$jsondecode["username"]?>!</h1>
            <p>List of DB users</p></div>
        
        <div><a href="logout.php">Logout</a></div>
        <div><a href="update_user.php">Settings</a></div>
        <a href="signup.php"><input type="button" name="signup" value="Add a user"/></a>  
        <style>
            .user_table, tr, td, th{
                
                border: 1px solid black;
                
            }
        
        </style>
    </head>
    <body>
        <table class="users_table">
            
            <tr>
                <th>Id</th>
                <th>Username</th>
                <th>Email</th>
                <th>Admin</th>

            </tr>
            <tr>
            <?php while($row = $requete->fetch(PDO::FETCH_ASSOC)):?>
                <td><?= $row['id'];?></td>
                <td><?= $row['username'];?></td>
                <td><?= $row['email'];?></td>
                <td><?= $row['admin'];?></td>
                <td><form action="update_user.php" method="GET">
                <input type="hidden" name="update_user" value="<?=$row['id']?>"/>
                <button type="submit">Update</button>
                </form></td>
                <td><form action="delete_user.php" method="GET">
                <input type="hidden" name="delete_user" value="<?=$row['id']?>"/>
                <button>Delete</button>
                </form></td>
            </tr>
            <?php endwhile;?>
            
            <a href="admin.php"><input type="button" name="valider" value="Back to admin page"/></a></br>
            <a href="index.php"><input type="button" name="valider" value="Back to index page"/></a>  
        </table>

    </body>
</html>