<?php
    
    $jsondecode=json_decode($_COOKIE["infos_user"], true);

    if($jsondecode["autoriser"]!= "oui" || $jsondecode["admin"]!= 1){
        header("location:index.php");
        exit;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin page</title>
        <div> <h1>Hello <?=$jsondecode["username"]?>!</h1>
            <p>Admin Page</p></div>
        
    </head>
    <body>
        <table class="admin">

            <tr>
                <td><form action="show_user.php" method="GET">
                <input type="hidden" name="show_user" value="<?=$row['id']?>"/>
                <button type="submit">Show Users</button>
                </form></td>
                <td><form action="show_products.php" method="GET">
                <input type="hidden" name="show_products" value="<?=$row['id']?>"/>
                <button type="submit">Show Products</button>
                </form></td>
            </tr>    
        </table>
    </body>
</html>