<?php


$jsondecode=json_decode($_COOKIE["infos_user"], true);

if($jsondecode["autoriser"]!= "oui"){
    header("location:index.php");
    exit;
}
    
CONST ERROR_LOG_FILE='errors.log';

    $DB_name='my_shop';
    $DB_user='cyril';
    $DB_password ='cyril';
    $DB_host= 'localhost';
    $DB_port= 3306;
    $DB_table='products';

try{       
    
    $connect = new PDO("mysql:host=".$DB_host.";port=".$DB_port.";dbname=".$DB_name, $DB_user, $DB_password);
    
    $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    if($connect){
        
        
        $requete=$connect->prepare("DELETE FROM products WHERE id=:id");
        
        $requete->execute([
            'id'=>$_GET['delete_product']
        ]);
        header("location:show_products.php");        
    }
}
catch(PDOException $e){
    
    $error_connect = "Error connection to DB\n";
    error_log(date("Y-m-d H:i")." -> ".$error_connect, 3,ERROR_LOG_FILE);
    echo $error_connect;
    $error_message = "PDO ERROR : ".$e->getMessage()." storage in ".ERROR_LOG_FILE."\n";
    error_log(date("Y-m-d H:i")." -> ".$error_message, 3,ERROR_LOG_FILE);
    exit;
}


    