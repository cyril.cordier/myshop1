<?php
    
    $jsondecode=json_decode($_COOKIE["infos_user"], true);

    if($jsondecode["autoriser"]!= "oui" || $jsondecode["admin"]!= 1){
        header("location:index.php");
        exit;
    }
    $DB_name='my_shop';
    $DB_products='cyril';
    $DB_password ='cyril';
    $DB_host= 'localhost';
    $DB_port= 3306;
    $DB_table='products';
    try{       
        
        $connect = new PDO("mysql:host=".$DB_host.";port=".$DB_port.";dbname=".$DB_name, $DB_products, $DB_password);
        
        $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if($connect){
            
            $sql="SELECT * FROM products";
            $requete=$connect->prepare($sql);
            
            $requete->execute();
                
            //$ligne = $requete->fetch(PDO::FETCH_ASSOC);
        
        

                //<?php
        //$pdo = new PDO('mysql:host='.$host.';dbname='.$db, $db_products, $db_password);
        //$sql = 'SELECT * FROM products';   
        //$req = $pdo->query($sql);  


            
            //$req->closeCursor();
        }
    



    }
    catch(PDOException $e){
    
        $error_connect = "Error connection to DB\n";
        error_log(date("Y-m-d H:i")." -> ".$error_connect, 3,ERROR_LOG_FILE);
        echo $error_connect;
        $error_message = "PDO ERROR : ".$e->getMessage()." storage in ".ERROR_LOG_FILE."\n";
        error_log(date("Y-m-d H:i")." -> ".$error_message, 3,ERROR_LOG_FILE);
        exit;
    }
    
    


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Show products page</title>
        <div> <h1>Hello <?=$jsondecode["username"]?>!</h1>
            <p>List of DB products</p></div>
        
        <div><a href="logout.php">Logout</a></div>
        <div><a href="update_product.php">Settings</a></div>
        <a href="add_product.php"><input type="button" name="Add_product" value="Add a product"/></a>  
        <style>
            .products_table, tr, td, th{
                
                border: 1px solid black;
            }
            .fit-picture{
                
                height:300px;
            }
        
        </style>
    </head>
    <body>
        <table class="products_table">
            
            <tr>
                <th>Id</th>
                <th>name</th>
                <th>Price</th>
                <th>Category product</th>
                <th>Description</th>
                <th>Picture</th>

            </tr>
            <tr>
            <?php while($row = $requete->fetch(PDO::FETCH_ASSOC)):?>
                <td><?= $row['id'];?></td>
                <td><?= $row['name'];?></td>
                <td><?= $row['price'];?></td>
                <td><?= $row['category_id'];?></td>
                <td><?= $row['description'];?></td>
                <td><img class="fit-picture" alt="picture_product" src="./pictures/<?= $row['picture'];?>"/></td>
                <td><form action="update_product.php" method="GET">
                <input type="hidden" name="update_product" value="<?=$row['id']?>"/>
                <button type="submit">Update</button>
                </form></td>
                <td><form action="delete_product.php" method="GET">
                <input type="hidden" name="delete_product" value="<?=$row['id']?>"/>
                <button>Delete</button>
                </form></td>
            </tr>
            <?php endwhile;?>
            <a href="admin.php"><input type="button" name="valider" value="Back to admin page"/></a></br>  
        </table>

    </body>
</html>