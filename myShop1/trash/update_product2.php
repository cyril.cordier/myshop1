<?php

        
$jsondecode=json_decode($_COOKIE["infos_user"], true);

if($jsondecode["autoriser"]!= "oui"){
    header("location:index.php");
    exit;
}
    
    @$new_name=$_POST["username"];
    @$new_email=$_POST["email"];
    @$new_password=$_POST["password"];
    @$password_confirmation=$_POST["password_confirmation"];
    @$setAdmin=$_POST["setAdmin"];
    @$valider=$_POST["valider"];
    $testmail=0;
    $testpwd=0;
    $testname=0;
    $erreur_name;
    $erreur_email;
    $erreur_password;
    $hash;
    
    CONST ERROR_LOG_FILE='errors.log';
    $jsondecode=json_decode($_COOKIE["infos_user"], true);
        $DB_name='my_shop';
        $DB_user='cyril';
        $DB_password ='cyril';
        $DB_host= 'localhost';
        $DB_port= 3306;
        $DB_table='products';

    try{       
        
        $connect = new PDO("mysql:host=".$DB_host.";port=".$DB_port.";dbname=".$DB_name, $DB_user, $DB_password);
        
        $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if($connect){
            
            
            $requete=$connect->prepare("SELECT * FROM products WHERE id=:id");
            
            $requete->execute([
                'id'=>$_GET['update_product']
                ]);
                
            $ligne = $requete->fetch(PDO::FETCH_ASSOC);
            $info_product=array('name'=>$ligne['name'], 'price'=>$ligne['price'], 'category_id'=>$ligne['category_id'], 'description'=>$ligne['description'], 'picture'=>$ligne['picture']);
        }
    }
    catch(PDOException $e){
        
        $error_connect = "Error connection to DB\n";
        error_log(date("Y-m-d H:i")." -> ".$error_connect, 3,ERROR_LOG_FILE);
        echo $error_connect;
        $error_message = "PDO ERROR : ".$e->getMessage()." storage in ".ERROR_LOG_FILE."\n";
        error_log(date("Y-m-d H:i")." -> ".$error_message, 3,ERROR_LOG_FILE);
        exit;
    }

    function modifyProduct(array $update, $ligne, $connect)
    { 
        if ($update['admin'] != "yes"){
            $isAdmin=0;
        }else{
            $isAdmin=1;
        }
        if($update['password'] == $ligne['password'])
        {
            $newhash=$update['password'];
        }
        else{
            $newhash = password_hash($update['password'], PASSWORD_BCRYPT);
        }
        $modified_at=date('Y-m-d H:i');
        $connection=$connect->prepare("UPDATE products SET 
        name=:name, 
        email=:email, 
        modified_at=:modify_at,
        password=:password,
        admin=:isadmin 
        WHERE id=:id;");
        $connection->execute([
            'id'=>$_GET['update_user'],
            'username'=>$update['username'],
            'email'=>$update['email'],
            'modify_at'=>$modified_at,
            'password'=>$newhash,
            'isadmin'=>$isAdmin
        ]); 
        $info_user=array('username'=>$update['username'], 'email'=>$update['email'], 'admin'=>$isAdmin);
        header("location:admin.php");
    }
    if($_SERVER['REQUEST_METHOD']== 'POST')
    {
        if(isset($valider)){
            
            
            if(isset($new_name)&&(strlen($new_name)<3 || strlen($new_name)>10)){
                $erreur_name="Invalid name";
            }else{$testname=1;}
            if(isset($new_email)&&!preg_match ( " /^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/ " , $new_email)){
                $erreur_email="Invalid email";
            }else {$testmail=1;}

                if(empty($new_password) && empty($password_confirmation))
                {
                    $new_password=$ligne['password'];
                    $testpwd=1;
                    
                }else{

                    if(((strlen($new_password)<3 || strlen($new_password)>10) || ($new_password!==$password_confirmation))){
                    $erreur_password="Invalid password or password confirmation";
                    }else{
                        $testpwd=1;
                    }
                }
            if($testname==1 && $testmail==1 && $testpwd==1)
            {
                
                $update=array('username'=>$new_name, 'email'=>$new_email, 'password'=>$new_password, 'admin'=>$setAdmin);
                modifyUser($update, $ligne, $connect);
            }
        }
    }

?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Update User</title>     

    </head>
    <body>
    <div>
        <?php
            if (!empty($erreur_name)){?>
            <div id="erreur name">
                <?=$erreur_name?>
            </div>
            <?php 
            }
            if (!empty($erreur_email)){?>
            <div id="erreur email">
                <?=$erreur_email?>
            </div>
            <?php 
            }
            if (!empty($erreur_password)){?>
                <div id="erreur password">
                    <?=$erreur_password?>
                </div>
            <?php 
            }?>
            
    </div>
            <style type="text/css">
            .calage{
                margin:0 20px 0;
                display:inline;
                width:130px;
                float:left;
            }
            </style>
        <br/>
        <form name="fo" action="" method="post">
            <label for="name" class="calage">Name :</label><input type="text" placeholder="Username" name="username" value="<?=$info_user['username']?>"/><br/>
            <label for="Email" class="calage">Email :</label><input type="text" placeholder="Email" name="email" value="<?=$info_user['email']?>"/><br/>
            <label for="Password" class="calage">Password :</label><input type="password" placeholder="Password" name="password" /><br/>
            <label for="Password conf" class="calage">Password Confirmation :</label><input type="password" placeholder="Password confirmation" name="password_confirmation"/><br/>
            <p><label for="setAdmin" class="calage">Set as Admin :</label><input type="checkbox" name="setAdmin" value="yes" <?php echo($info_user['admin']==1 ? 'checked' : ''); ?>><br/></p>
            <a href="modify_account.php"><input type="submit" name="valider" value="Modify"/></a>
            <a href="admin.php"><input type="button" name="valider" value="Back to admin page"/></a></br>
            <a href="index.php"><input type="button" name="valider" value="Back to index page"/></a>  
        </form>
       

    </body>
</html>