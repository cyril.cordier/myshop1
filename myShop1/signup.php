<?php

    
    @$name=$_POST["name"];
    @$email=$_POST["email"];
    @$password=$_POST["password"];
    @$password_confirmation=$_POST["password_confirmation"];
    @$valider=$_POST["valider"];
    @$setAdmin=$_POST["setAdmin"];
    CONST ERROR_LOG_FILE='errors.log';


    class Inscription{
		
        
        public $erreur_name;
        public $erreur_email;
        public $erreur_password;
        public $hash;
        public $testmail;
        public $test=0;
        public $isAdmin;
        public CONST ERROR_LOG_FILE='errors.log';

        function createUser($name, $email, $password, $setAdmin)
        {
            $DB_name='my_shop';
            $DB_user='cyril';
            $DB_password ='cyril';
            $DB_host= 'localhost';
            $DB_port= 3306;
            $DB_table='users';           
        
            try{       
                $hash=password_hash($password, PASSWORD_BCRYPT);
                $jsondecode=json_decode($_COOKIE["infos_user"], true);
                if($jsondecode["autoriser"]!= "oui"){
                    if ($setAdmin != "setAdmin"){
                        $isAdmin=0;
                    }else{
                        $isAdmin=1;
                    }
                }
                $connect = new PDO("mysql:host=".$DB_host.";port=".$DB_port.";dbname=".$DB_name, $DB_user, $DB_password);
                $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                if($connect){
                    $exist_name=$connect->prepare("SELECT username FROM users where username=:username;");
                    $exist_name->execute([
                        'username'=>$name
                        ]);
                    $exist_email=$connect->prepare("SELECT email FROM users where email=:email;");
                    $exist_email->execute([
                    'email'=>$email
                    ]);
                    if(!empty($ligne = $exist_name->fetch(PDO::FETCH_ASSOC))){
                        echo "Username already exists"."\n";
                        $test=1;
                    }if(!empty($ligne = $exist_email->fetch(PDO::FETCH_ASSOC))){
                        echo "Email adress already exists"."\n";
                        $test=1;
                    }if($test==0){
                        $created_at=date('Y-m-d');
                        $connection=$connect->prepare("INSERT INTO users (username,password,email, admin, created_at) VALUES  (
                            '$name',
                            '$hash',
                            '$email',
                            '$isAdmin',
                            '$created_at'  
                            );")->execute();    
                        echo "User created";
                        header("Refresh:2;url=signin.php");
                    }
                }    
            }
            catch(PDOException $e){
            
                $error_connect = "Error connection to DB\n";
                error_log(date("Y-m-d H:i")." -> ".$error_connect, 3,ERROR_LOG_FILE);
                echo $error_connect;
                $error_message = "PDO ERROR : ".$e->getMessage()." storage in ".ERROR_LOG_FILE."\n";
                error_log(date("Y-m-d H:i")." -> ".$error_message, 3,ERROR_LOG_FILE);
                exit;
            }  
        }
    }    
          
    if($_SERVER['REQUEST_METHOD']== 'POST')
    {
        if(isset($valider)){
            if(isset($name)&&(strlen($name)<3 || strlen($name)>10)){
                $erreur_name="Invalid name";
            }else{$testname=1;}
            if(isset($email)&&!preg_match ( " /^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/ " , $email)){
                $erreur_email="Invalid email";
            }else {$testmail=1;}
            if(isset($password) && isset($password_confirmation) && ((strlen($password)<3 || strlen($password)>10) || ($password!==$password_confirmation))){
                $erreur_password="Invalid password or password confirmation";
            }else {$testpwd=1;}
            if($testname==1 && $testmail==1 && $testpwd==1)
            {
                $go=new Inscription();
                $go->createUser($name, $email, $password, $setAdmin);
            }
        }
    }
        
    





?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Create your account</title>     

    </head>
    <body>
    <div>
        <?php
            if (!empty($erreur_name)){?>
            <div id="erreur name">
                <?=$erreur_name?>
            </div>
            <?php 
            }
            if (!empty($erreur_email)){?>
            <div id="erreur email">
                <?=$erreur_email?>
            </div>
            <?php 
            }
            if (!empty($erreur_password)){?>
                <div id="erreur password">
                    <?=$erreur_password?>
                </div>
            <?php 
            }?>
            
    </div>
            <style type="text/css">
            .calage{
                margin:0 20px 0;
                display:inline;
                width:130px;
                float:left;
            }
            </style>
        <br/>
        <form name="fo" action="" method="post">
            <label for="name" class="calage">Name :</label><input type="text" placeholder="Name" name="name"/><br/>
            <label for="Email" class="calage">Email :</label><input type="text" placeholder="Email" name="email"/><br/>
            <label for="Password" class="calage">Password :</label><input type="password" placeholder="Password" name="password" /><br/>
            <label for="Password conf" class="calage">Password Confirmation :</label><input type="password" placeholder="Password confirmation" name="password_confirmation"/><br/>       
            
            <input type="submit" name="valider" value="Submit"/>
             
        </form>
       

    </body>
</html>