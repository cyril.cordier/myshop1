<?php

    
    @$name=$_POST["username"];
    @$email=$_POST["email"];
    @$password=$_POST["password"];
    @$password_confirmation=$_POST["password_confirmation"];
    @$valider=$_POST["valider"];


	
    $erreur_name;
    $erreur_email;
    $erreur_password;
    $hash;
    $testmail;
    CONST ERROR_LOG_FILE='errors.log';

    function createUser($name, $email, $password)
    {
        $DB_name='my_shop';
        $DB_user='cyril';
        $DB_password ='cyril';
        $DB_host= 'localhost';
        $DB_port= 3306;
        $DB_table='users';
        
    
        try{       
            $hash=password_hash($password, PASSWORD_BCRYPT);
            $connect = new PDO("mysql:host=".$DB_host.";port=".$DB_port.";dbname=".$DB_name, $DB_user, $DB_password);
            
            $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            if($connect){
                
                $created_at=date('Y-m-d');
                
                $connection=$connect->prepare("INSERT INTO users (username,password,email, created_at) VALUES  (
                    '$name',
                    '$hash',
                    '$email',
                    '$created_at'  
                    );")->execute();    
            echo "User created";
            }
        }
        catch(PDOException $e){
        
            $error_connect = "Error connection to DB\n";
            error_log(date("Y-m-d H:i")." -> ".$error_connect, 3,ERROR_LOG_FILE);
            echo $error_connect;
            $error_message = "PDO ERROR : ".$e->getMessage()." storage in ".ERROR_LOG_FILE."\n";
            error_log(date("Y-m-d H:i")." -> ".$error_message, 3,ERROR_LOG_FILE);
            exit;
        }  
    }
    if($_SERVER['REQUEST_METHOD']== 'POST')
    {
        if(isset($valider)){
            if(isset($name)&&(strlen($name)<3 || strlen($name)>10)){
                $erreur_name="Invalid name";
            }else{$testname=1;}
            if(isset($email)&&!preg_match ( " /^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/ " , $email)){
                $erreur_email="Invalid email";
            }else {$testmail=1;}
            if(isset($password) && isset($password_confirmation) && ((strlen($password)<3 || strlen($password)>10) || ($password!==$password_confirmation))){
                $erreur_password="Invalid password or password confirmation";
            }else {$testpwd=1;}
            if($testname==1 && $testmail==1 && $testpwd==1)
            {
                createUser($name, $email, $password);
            }
        }
    }

class Classe{
	public function __construct($name, $password)
	{
		echo "Hello $name";
		echo "Hello $password";
	}
}




$go= new Classe($name, $password)

?>

<!--
Author: Colorlib
Author URL: https://colorlib.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>SignUp Form</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- //Custom Theme files -->
<!-- web font -->
<link href="//fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i" rel="stylesheet">
<!-- //web font -->
</head>
<body>
	<div>
		<?php
			if (!empty($erreur_name)){?>
			<div id="erreur name">
				<?=$erreur_name?>
			</div>
			<?php 
			}
			if (!empty($erreur_email)){?>
			<div id="erreur email">
				<?=$erreur_email?>
			</div>
			<?php 
			}
			if (!empty($erreur_password)){?>
				<div id="erreur password">
					<?=$erreur_password?>
				</div>
			<?php 
			}?>
			
	</div>	

	<!-- main -->
	<div class="main-w3layouts wrapper">
		<h1>SignUp Form</h1>
		<div class="main-agileinfo">
			<div class="agileits-top">
				<form action="#" method="post">
					<input class="text" type="text" name="username" placeholder="Username" required="">
					<input class="text email" type="email" name="email" placeholder="Email" required="">
					<input class="text" type="password" name="password" placeholder="Password" required="">
					<input class="text w3lpass" type="password" name="password_confirmation" placeholder="Confirm Password" required="">
					<div class="wthree-text">
						<label class="anim">
							<input type="checkbox" class="checkbox">
							<span>Remember me</span>
						</label>
						<div class="clear"> </div>
					</div>
					<input type="submit" name="valider" value="SIGNUP">
				</form>
				<p>Don't have an Account? <a href="#"> Login Now!</a></p>
			</div>
		</div>
		<!-- copyright -->
		<div class="colorlibcopy-agile">
			<p>© 2018 Colorlib Signup Form. All rights reserved | Design by <a href="https://colorlib.com/" target="_blank">Colorlib</a></p>
		</div>
		<!-- //copyright -->
		<ul class="colorlib-bubbles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
	<!-- //main -->
</body>
</html>