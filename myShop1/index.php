<?php
    $DB_name='my_shop';
    $DB_products='cyril';
    $DB_password ='cyril';
    $DB_host= 'localhost';
    $DB_port= 3306;
    $DB_table='products';
    try{       
        
        $connect = new PDO("mysql:host=".$DB_host.";port=".$DB_port.";dbname=".$DB_name, $DB_products, $DB_password);
        
        $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if($connect){
            
            $sql="SELECT * FROM products";
            $requete=$connect->prepare($sql);
            
            $requete->execute();

        }
    



    }
    catch(PDOException $e){
    
        $error_connect = "Error connection to DB\n";
        error_log(date("Y-m-d H:i")." -> ".$error_connect, 3,ERROR_LOG_FILE);
        echo $error_connect;
        $error_message = "PDO ERROR : ".$e->getMessage()." storage in ".ERROR_LOG_FILE."\n";
        error_log(date("Y-m-d H:i")." -> ".$error_message, 3,ERROR_LOG_FILE);
        exit;
    }
    
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1 shrink-to-fit=no">
    <title>E-shop team#9</title>
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="styles/styles.css">
</head>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <img id="menu" alt="Menu principal" src="images/logo_2.png" width=71px height=71px/>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                
              
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                  <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                      <a class="nav-link" href="#">HOME <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">SHOP</a>
                    </li>
                  </ul>
                  
                  <form action="" class="formulaire">
                    <button id="search" action="submit" name="search"><img  alt="loupe"  src="images/search.png"/></button>
                    <input class="champ" type="text" Placeholder="Search a product"/>
                </form>
                  <img class=Panier id="panier" alt="panier" src="images/cart-button.png"/>
                  <li class="nav-item">
                    <a class="nav-link" href="signin.php">LOGIN</a>
                  </li>
                </div>
              </nav>
            
    </div>    
        
        <!-- <div class=barresearch>
            
            <div class=recherche>
                <form action="" class="formulaire">
                    <input class="champ" type="text" value="living room"/>
                </form>
                <div class=Powered>Powered by <strong>Algolia</strong></div><img class=power alt="Power" src="images/logoPower.png"/>
            </div>
            <div class=filtre>
                <ul class=Orderby> 
                    <li><a>Best match</a><img  class=arrow alt="arrow" src="images/arrow.png" width=16px height=8px/></li>
                </ul>
                
            </div>

        </div> -->
        
        

        
    <div class="container-fluid">
        <div class="grids">
            <div class="row">


                <!-- <div class=card>
                    <div>
                        <p class=filterby >FILTER BY</p>
                        
                    </div>
                    <div class=filter>
                        <p class=filter_type>Collection<img class=filter_arrow alt="arrow" src="images/arrow.png" width=8px height=5px/></p>
                        <p class=filter_type>Color<img class=filter_arrow alt="arrow" src="images/arrow.png" width=8px height=5px/></p>
                        <p class=filter_type>Category<img class=filter_arrow alt="arrow" src="images/arrow.png" width=8px height=5px/></p>
                        <p class=filter_price>Price Range</p>
                        
                            <img class=knobR alt="knobR" src="images/knob-left.png"/>
                            
                            <img class=knobL alt="knobL" src="images/knob-right.png"/>
                    
                        <div class=price_filter>
                            <div class=price_min>$0</div>
                            <div class=price_max>$10,000+</div>
                        </div>
                    </div>
                    
                </div> -->
                <?php while($ligne = $requete->fetch(PDO::FETCH_ASSOC)):?>
                    <div class="card col-xs-12 col-md-4 col-lg-3"style="width: 12rem;">
                        <div><img class="img-responsive"  alt="product" src="./pictures/<?= $ligne['picture'];?>" height:300px/></div>
                        <div class="card-body">
                            <div>
                                <p class="card-title"><?= $ligne['name'];?></p> 
                                <p class="card-text"><?= $ligne['category_id'];?></p>
                                <p class="card-text"><?= $ligne['description'];?></p>
                            </div>
                            <div>
                                
                                <p class="card-price"><?= $ligne['price'];?> €</p>
                                
                                <img class=cart alt="panier" src="images/add-to-cart-button.png"/>
                            </div>
                        </div>
                        
                    </div>
                <?php endwhile;?>           
        

            </div>        
        </div>
    </div>
  
      

</body>
    <footer>
        <div class=pagenumber>
            <div class=number_selected>1</div>
            <div class=number_unselected>2</div>
            <div class=number_unselected>3</div>
            <div class=number_unselected>4</div>
            <div class=number_unselected>5</div>
            <div class=number_unselected>6</div>
            <div class=number_unselected>7</div>
            <div class=number_unselected>8</div>
            <div class=number_unselected>9</div>
            <div class=number_unselected>10</div>

            <img class=end alt="page_number" src="images/number-unselected.png"/>

        </div>
    </footer>
        <!--JS, Popper.js, and jQuery-->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</html>



