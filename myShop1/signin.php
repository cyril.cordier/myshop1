<?php

    //session_start();
    @$email=$_POST["email"];
    @$password=$_POST["password"];
    @$remember=$_POST["rememberme"];
    @$valider=$_POST["valider"];
    

    $erreur_email;
    $erreur_password;
    $hash;
    $testmail;

    
    
    function login($login, $password, $remember)
    {
        $DB_name='my_shop';
        $DB_user='cyril';
        $DB_password ='cyril';
        $DB_host= 'localhost';
        $DB_port= 3306;
        $DB_table='users';
        
        
        try{
            $connect = new PDO("mysql:host=".$DB_host.";port=".$DB_port.";dbname=".$DB_name, $DB_user, $DB_password);
            $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            if($connect){
                $requete=$connect->prepare("SELECT * FROM users WHERE email=:mail;");
                $requete->execute([
                    'mail'=>$login
                    ]);

                $ligne = $requete->fetch(PDO::FETCH_ASSOC);
                //echo $ligne["id"]." ".$ligne["name"]." ".$ligne["email"]."\n";
            }
             
           if($login==$ligne["email"] && password_verify($password, $ligne["password"]))
            {
                
                if ($remember != "getremember"){
                    $expirdate=0;
                }else{
                    $expirdate=(time()+365*24*3600);
                }

                $jsonforcookie=array(
                    "autoriser"=>"oui", 
                    "username"=>$ligne["username"], 
                    "email"=>$ligne["email"], 
                    "password"=>$ligne["password"], 
                    "id"=>$ligne["id"], 
                    "admin"=>$ligne["admin"],
                    "expirdate"=>$expirdate);

                /*echo $remember;
                echo $expirdate;*/
                setcookie("infos_user", json_encode($jsonforcookie) , $expirdate);
                
                
                // connecté, redirection vers admin.php
                header("Location: admin.php");
                //exit;
            }else{
                //pas connecté, reste sur la page login
                echo "Incorrect email/password";
            }
                
                
        }
    
        catch(Exception $e){
            echo $e->getMessage();
            
        }
    
    }

    if($_SERVER['REQUEST_METHOD']== 'POST')
    {
        if(isset($valider)){
            if(isset($email)&&!preg_match ( " /^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/ " , $email)){
                $erreur_email="Invalid email";
            }else {$testmail=1;}
            if(is_null($password)){
                $erreur_password="Invalid password";
            }else {$testpwd=1;}
            if($testmail==1 && $testpwd==1)
            {
                login($email, $password, $remember);
            }
        }
    }

?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Log in to your account</title>     

    </head>
    <body>
    <div>
        <?php
            if (!empty($erreur_email)){?>
            <div id="erreur email">
                <?=$erreur_email?>
            </div>
            <?php 
            }
            if (!empty($erreur_password)){?>
                <div id="erreur password">
                    <?=$erreur_password?>
                </div>
            <?php 
            }?>
            
    </div>
            <style type="text/css">
            .calage{
                margin:0 20px 0;
                display:inline;
                width:130px;
                float:left;
            }
            </style>
        <br/>
        <form name="fo" action="" method="post">
            <label for="Email" class="calage">Email :</label><input type="text" placeholder="Email" name="email"/><br/>
            <label for="Password" class="calage">Password :</label><input type="password" placeholder="Password" name="password" /><br/>
            <p><label for="rememberme" class="calage">Remember me :</label><input type="checkbox" id="checkremember" name="rememberme" value="getremember"><br/></p>
            <input type="submit" name="valider" value="Submit"/>
            <a href="index.php"><input type="button" name="valider" value="Back to index page"/></a>  
            <a href="admin.php"><input type="button" name="valider" value="Go to admin page"/></a>  
             
        </form>
       

    </body>
</html>