<?php

require 'Autoloader.php';


class DB{

    public CONST ERROR_LOG_FILE='../ERRORLOG/errors.log';
    public $connect;

    public function __construct(/*$DBhost, $DBport, $DBname, $DBuser, $DBpassword*/){
        
        try{       
            $go=new Config();
            $DB_host=$go->getHost();
            $DB_port=$go->getPort();
            $DB_name=$go->getName();
            $DB_user=$go->getUser();
            $DB_password=$go->getPassword();
            $connect = new PDO("mysql:host=".$DB_host.";port=".$DB_port.";dbname=".$DB_name, $DB_user, $DB_password);
            $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo "connected to DB\n";
            return $connect;
                
        }
        catch(PDOException $e){
        
            $error_connect = "Error connection to DB\n";
            error_log(date("Y-m-d H:i")." -> ".$error_connect, 3,DB::ERROR_LOG_FILE);
            echo $error_connect;
            $error_message = "PDO ERROR : ".$e->getMessage()." storage in ".DB::ERROR_LOG_FILE."\n";
            error_log(date("Y-m-d H:i")." -> ".$error_message, 3,DB::ERROR_LOG_FILE);
            exit;
        }  
    }

    public function action($action, $table, array $where){ //[0] -> name , [1] -> = , [2] -> serge

        if(count($where) === 3){
            $operators = array('=', '<', '>', '<>', '<=', '>=', '!=');
            $column = $where[0]; 
            $operator = $where[1];
            $value = $where[2];

            if(in_array($operator, $operators)){
                $sql = "{$action} FROM {$table} WHERE {$column} {$operator} {$value}";
            }
            /*$query=$sql->fetch(PDO::FETCH_ASSOC);
            echo $query["username"];*/
        }
    }

    public function get($table, $where){
    
        return $this->action('SELECT *', $table, $where);
        
    }




} 


$user =  new DB();

$query= $user->get('users', array('username', '=', 'christine'));
$recherche=$user->query($query);
$ligne = $recherche->fetch(PDO::FETCH_ASSOC);
 echo $ligne["id"]." ".$ligne["username"]." ".$ligne["email"]."\n";