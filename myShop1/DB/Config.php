<?php

class Config{
    public $DB_name;
    public $DB_user;
    public $DB_password;
    public $DB_host;
    public $DB_port;
    public $DB_table;
    public $connectinfo;

    public function __construct(){
        $DB_name='my_shop';
        $DB_user='cyril';
        $DB_password ='cyril';
        $DB_host= 'localhost';
        $DB_port= 3306;
        $DB_table='users';
        
        $this->DB_name=$DB_name;
        $this->DB_user=$DB_user;
        $this->DB_password=$DB_password;
        $this->DB_host=$DB_host;
        $this->DB_port=$DB_port;
        $this->DB_table=$DB_table;

        //return $connectinfo = array('DBname'=>$this->DB_name, 'DBuser'=>$this->DB_user, 'DBpassword'=>$this->DB_password, 'DBhost'=>$this->DB_host, 'DBport'=>$this->DB_port, 'Dtable'=>$this->DB_table);
    }

    public function getName(){return $this->DB_name;}
    public function getUser(){return $this->DB_user;}
    public function getPassword(){return $this->DB_password;}
    public function getHost(){return $this->DB_host;}
    public function getPort(){return $this->DB_port;}
    public function getTable(){return $this->DB_table;}
}

